import boto3
import datetime

ACCESS_KEY = 'AKIAIZHZIGKWSXRJHHZQ'
SECRET_KEY = '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'

sqs = boto3.resource(
    'sqs',
    aws_access_key_id = ACCESS_KEY,
    aws_secret_access_key = SECRET_KEY,
    region_name = 'us-west-2'
)

queue = sqs.get_queue_by_name(QueueName = 'testqueue.fifo')

str_timestamp = datetime.datetime.utcnow().strftime("%b %d %Y %H:%M:%S")

# print(str_timestamp)

response = queue.send_message(
    MessageBody='TEST', 
    MessageGroupId='2', 
    MessageDeduplicationId='2', 
    MessageAttributes={
        'timestamp': {
            'StringValue': str_timestamp,
            'DataType': 'String'
        }
    }
)

print('Message Sent')

log_message = open('log.txt','w')

def process(timestamp):
    current_time = datetime.datetime.utcnow().strftime("%b %d %Y %H:%M:%S")
    concat_message = '{0} {1}'.format(timestamp, current_time)
    print(concat_message)
    log_message.write(concat_message)

for message in queue.receive_messages(MaxNumberOfMessages=10,MessageAttributeNames=['timestamp']):
    if message.message_attributes is not None:
        timestamp = message.message_attributes.get('timestamp').get('StringValue')
        if timestamp:
            process(timestamp)

log_message.close()

print('Message Received')

client = boto3.client(
    'sns',
    aws_access_key_id = ACCESS_KEY,
    aws_secret_access_key = SECRET_KEY,
    region_name = 'us-west-2'
)

client.publish(TopicArn = 'arn:aws:sns:us-west-2:481929136776:TestSNS', Message="complete")

print('SMS SENT TO SNS TOPIC')